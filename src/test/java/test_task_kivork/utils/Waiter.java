package test_task_kivork.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import test_task_kivork.TestProperties;

import java.util.List;

public class Waiter {

    private WebDriverWait waiter = null;
    int DEFAULT_WAIT_TIME = Integer.valueOf(TestProperties.getProperties().getProperty("test.driver.waiter.timeout"));

    public Waiter(WebDriver driver) {
        if (waiter == null)
            waiter = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
    }

    public void waitForElement(WebElement element) {
        waiter.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForInvisibilityElement(WebElement element) {
        waiter.until(ExpectedConditions.invisibilityOf(element));
    }

    public void waitForElementsList(List<WebElement> elements) {
        waiter.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public void waitVisibilityOfElementLocated(By locator) {
        waiter.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForTextToBePresentInElement(WebElement element, String text) {
        waiter.until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    public void waiterMilliseconds(Integer milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void waitForUrlToBe(String urlToBe) {
        waiter.until(ExpectedConditions.urlToBe(urlToBe));
    }

    public boolean isElementDisplayed(WebElement element) {
        boolean result = false;
        if (element.isDisplayed()) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public boolean waitForJSandJQueryToLoad() {

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long)((JavascriptExecutor)driver).executeScript("return jQuery.active") == 0);
                }
                catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return waiter.until(jQueryLoad) && waiter.until(jsLoad);
    }

    public void waitForPreloaderInvisibility(WebElement preloader){
        try{
            if(preloader.isDisplayed()){
                waiter.until(ExpectedConditions.invisibilityOf(preloader));
            }
        } catch (Exception e){}
        waitForJSandJQueryToLoad();
    }

    public void waitForElementAndClick(WebElement element) {
        waitForJSandJQueryToLoad();
        waitForElement(element);
        element.click();
    }

    public void assertElementVisibility(WebElement assertElement){
        Assert.assertTrue(
                assertElement.isDisplayed(),
                "Element is not visible on page! LOCATOR -> " + assertElement
        );
    }

    public void assertTextInElement(WebElement element, String expectedText){
        Assert.assertEquals(element.getText(), expectedText);
    }

    public void assertTextInElementAttribute(WebElement element, String attribute, String expectedText){
        Assert.assertEquals(element.getAttribute(attribute), expectedText);
    }

    public void waitForElementAndAssertIt(WebElement element){
        waitForJSandJQueryToLoad();
        waitForElement(element);
        assertElementVisibility(element);
    }

    public boolean waitForElementsAndAssertCount(List<WebElement> elements, int count) {
        waitForJSandJQueryToLoad();
        waitForElementsList(elements);
        boolean result = false;
        if(elements.size() == count) {
            result = true;
        }
        else {
            result = false;
        }
        return result;
    }
}
