package test_task_kivork.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import test_task_kivork.utils.Waiter;

public class AuthPage extends AbstractPage{

    public AuthPage(WebDriver driver) {
        super(driver);
    }

    Waiter waiter = new Waiter(driver);

    @FindBy(xpath = "//a[contains(@class, 'js-header-login-link')]//span[contains(text(), 'Войти в аккаунт')]")
    WebElement loginButton;

    @FindBy(xpath = "//a[@class='bui-button bui-button--light bui-traveller-header__product-action']")
    WebElement regObjectButton;

    @FindBy(xpath = "//a[@data-track-ga='click_header_logo_to_homepage']")
    WebElement bookingLink;

    @Step("Wait and click login button")
    public void waitWhenPageDOMLoadAndClick(){
        waiter.waitForElementAndClick(loginButton);
    }

    @Step("Click registration button")
    public void clickRegistrationButtonAndSwitchNextTab(){
        waiter.waitForElementAndClick(regObjectButton);
        switchANewTabInBrowser(0);
        waiter.waitForJSandJQueryToLoad();
    }
}
