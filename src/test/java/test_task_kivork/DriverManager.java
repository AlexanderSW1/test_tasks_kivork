package test_task_kivork;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static boolean isRemoteTest = true;
    protected static WebDriver driver;
    private static String selenoidHost = TestProperties.getProperties().getProperty("selenoidHost");
    private String booking = TestProperties.getProperties().getProperty("bookingURL");

    @BeforeClass(alwaysRun=true)
    @Parameters(value = {"browser"})
    public void beforeClassSetUp(String browser){
        System.out.println("*******************");
        openAndPrepareBrowser(browser);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(){
        try {
        }catch (Exception e){
            DesiredCapabilities dc = new DesiredCapabilities();
            dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT_AND_NOTIFY);
            System.out.println("Unexpected alert open");
        }
    }

    @BeforeMethod(alwaysRun=true)
    public void beforeTestSetUp (){
        System.out.println("-------------------------------------------------------\n" +
                "Next test started. Start Time - " + LocalTime.now());
        driver.get(booking);
    }

    public static void openAndPrepareBrowser(String browser) {

        isRemoteTest = Boolean.parseBoolean(TestProperties.getProperties().getProperty("isRemoteTrue"));
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);

        if (isRemoteTest == true) {
            driver = getRemoteDriver(selenoidHost, browser);
        } else {
            System.out.println("Local Driver start");
            driver = getLocalDriver();
            driver.manage().window().maximize();
        }
        driver.manage().deleteAllCookies();
    }

    @AfterMethod(alwaysRun=true)
    public void afterMethod(ITestResult result){
        String res = "PASSED";
        String methodName = result.getMethod().getMethodName();
        System.out.println("Method name - " + methodName);
        if(!result.isSuccess()){
            try {
                makeScreenshotOnFailure(result);
                res = "---FAILED---";
            }catch (Exception e){
                DesiredCapabilities dc = new DesiredCapabilities();
                dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT_AND_NOTIFY);
                System.out.println("Unexpected alert open");
                driver.get(booking);
            }
        }
        System.out.println("Test " + methodName
                + ". End Time - " + LocalTime.now() + "\t\t\t" + res);
    }

    @AfterClass(alwaysRun=true)
    public void tearDown() {
        if(driver!=null) {
            System.out.println("Closing browser");
            driver.quit();
        }
    }

    private static WebDriver getLocalDriver() {
        System.setProperty("webdriver.chrome.driver", TestProperties.getProperties().getProperty("test.driver.location"));
        String downloadFilePath = TestProperties.getProperties().getProperty("downloadFilePathLocal");
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", downloadFilePath);
        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless");
        options.setExperimentalOption("prefs", chromePrefs);
        return new ChromeDriver(options);
    }

    private static WebDriver getRemoteDriver(String selenoidHost, String browser) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        WebDriver driver = null;

        String downloadFilePath = TestProperties.getProperties().getProperty("downloadFilePath");

        if (browser.equals("chrome")) {
            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
            chromePrefs.put("download.default_directory", downloadFilePath);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);
            capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            capabilities.setCapability("enableVNC", true);
            capabilities.setVersion("1");
        }
        else if (browser.equals("firefox")) {
            capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability("enableVNC", true);
            capabilities.setVersion("1");
        }
        else if (browser.equals("android")) {
            capabilities = DesiredCapabilities.android();
            capabilities.setCapability("enableVNC", true);
        }
        else if (browser.equals("safari")) {
            capabilities = DesiredCapabilities.safari();
            capabilities.setCapability("enableVNC", true);
        }

        if (!browser.equals("safari")) {
            try {
                driver = new RemoteWebDriver(new URL("http://jenkins.jenkins.svc.cluster.local:8080/"), capabilities);
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                driver.manage().window().setSize(new Dimension(1920, 1080));
            } catch (MalformedURLException e) {
                org.testng.Assert.fail("Badly formed URL " + selenoidHost + ":4444/wd/hub");
            }
        }
        else {
            try {
                driver = new RemoteWebDriver(new URL("http://jenkins.jenkins.svc.cluster.local:8080/"), capabilities);
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                driver.manage().window().maximize();
            } catch (MalformedURLException e) {
                org.testng.Assert.fail("Badly formed URL http://10.10.21.167:4444/wd/hub");
            }
        }

        return driver;
    }

    public void makeScreenshotOnFailure(ITestResult result) {
        String methodName = result.getMethod().getMethodName();
        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshotFile, new File("Screenshot/" + methodName + ".jpg").getAbsoluteFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
