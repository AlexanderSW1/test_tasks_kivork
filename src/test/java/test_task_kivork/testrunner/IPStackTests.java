package test_task_kivork.testrunner;

import org.testng.annotations.Test;
import test_task_kivork.DriverManager;
import test_task_kivork.testsuites.SuiteIPStackPage;

public class IPStackTests extends DriverManager {

    SuiteIPStackPage suiteIPStackPage = new SuiteIPStackPage();

    @Test(enabled = true, groups = {"IP stack tests"})
    public void sendAPIRequestAndCheckIt(){
        suiteIPStackPage.sendAPIRequestAndCheckIt(driver);
    }
}
