package test_task_kivork.testrunner;

import org.testng.annotations.Test;
import test_task_kivork.DriverManager;
import test_task_kivork.testsuites.SuiteAuthPage;

public class BookingTests extends DriverManager {

    SuiteAuthPage suiteAuthPage = new SuiteAuthPage();

    @Test(enabled = true, groups = {"Login tests"})
    public void assertWhenPageDOMLoad(){
        suiteAuthPage.waitWhenPageDOMLoadAndClick(driver);
    }

    @Test(enabled = true, groups = {"Registration object tests"})
    public void clickRegistrationButton(){
        suiteAuthPage.clickRegistrationButton(driver);
    }
}
