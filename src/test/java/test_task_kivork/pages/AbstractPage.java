package test_task_kivork.pages;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.Cookies;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import test_task_kivork.TestProperties;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static io.restassured.config.RedirectConfig.redirectConfig;

public class AbstractPage {

    protected WebDriver driver;

    protected WebDriverWait wait;

    public AbstractPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Step("Switch a new tab in browser")
    public void switchANewTabInBrowser(Integer index){
        String oldTab = driver.getWindowHandle();
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        newTab.remove(oldTab);
        driver.switchTo().window(newTab.get(index));
    }

    @Step("Get cookie")
    public List getCookie(){
        Set<Cookie> seleniumCookies = driver.manage().getCookies();

        // This is where the Cookies will live going forward
        List restAssuredCookies = new ArrayList();

        // Simply pull all the cookies into Rest-Assured
        for (org.openqa.selenium.Cookie cookie : seleniumCookies) {
            restAssuredCookies.add(new io.restassured.http.Cookie.Builder(cookie.getName(), cookie.getValue()).build());
        }

        return restAssuredCookies;
    }

    @Step("Add file")
    public void addFile(String fileName){
        try {
            OutputStream output = new FileOutputStream("src/test/resources/" + fileName + ".properties");
            Properties prop = new Properties();
            prop.store(output, null);
        }catch (Exception e){
            System.out.println("Can't create file");
        }
    }

    @Step("Add variable to exist file")
    public void addVariableToExistFile(String fileName,String variable, String text){
        try {
            FileWriter fw = new FileWriter("src/test/resources/" + fileName + ".properties", true);
            BufferedWriter bw = new BufferedWriter(fw);
            Properties prop = new Properties();
            bw.newLine();
            prop.setProperty(variable, text);
            prop.store(fw.append(text), null);
            bw.close();
            System.out.println(prop);
        }catch (Exception e){
            System.out.println( "Can't write text");
        }
    }

    @Step("Get file")
    public String getFile(String fileName, String variable){
        CompositeConfiguration config = new CompositeConfiguration();
        config.addConfiguration(new SystemConfiguration());
        try {
            config.addConfiguration(new PropertiesConfiguration("src/test/resources/" + fileName + ".properties"));
        }catch (Exception e){
            System.out.println("Can't get file");
        }
        String file = config.getString(variable);
        return file;
    }

    @Step("Post request, content type - application/json")
    public void postRequestJson(String requestBody, String requestMethod){
        RestAssured.baseURI = TestProperties.getProperties().getProperty("bookingURL");
        RequestSpecification request = given().config(RestAssured.config().redirect(redirectConfig().followRedirects(false)));

        List restAssuredCookies = getCookie();
        System.out.println("restAssuredCookies - " + restAssuredCookies);

        Response response = request
                .contentType("application/json")
                .cookies(new Cookies(restAssuredCookies))
                .header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(requestMethod);

        response.then().statusCode(200);
    }
}
