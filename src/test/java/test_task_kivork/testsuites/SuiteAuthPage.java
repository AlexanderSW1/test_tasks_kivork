package test_task_kivork.testsuites;

import org.openqa.selenium.WebDriver;
import test_task_kivork.pages.AuthPage;

public class SuiteAuthPage {

    public void waitWhenPageDOMLoadAndClick(WebDriver driver){
        AuthPage authPage = new AuthPage(driver);

        authPage.waitWhenPageDOMLoadAndClick();
    }

    public void clickRegistrationButton(WebDriver driver){
        AuthPage authPage = new AuthPage(driver);

        authPage.clickRegistrationButtonAndSwitchNextTab();
    }
}
