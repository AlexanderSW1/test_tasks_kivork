package test_task_kivork.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Random;

public class BaseUtils {

    public void scrollDown(WebDriver driver, int value) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,"+ value +")", "");
    }

    /**
     *
     * @param driver - WebDriver
     * @param element - WebElement
     */
    public void scrollDownToElement(WebDriver driver, WebElement element) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public String getRandomName(){
        String randomName = "Test";
        String alphabet= "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        for (int i = 0; i < 8; i++) {
            char c = alphabet.charAt(random.nextInt(26));
            randomName+=c;
        }
        return randomName;
    }

    public String getRandomEmail(){
        String randomEmail = "testEmail";
        String alphabet= "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        for (int i = 0; i < 8; i++) {
            char c = alphabet.charAt(random.nextInt(26));
            randomEmail+=c;
        }
        return randomEmail;
    }

    /**
     *
     * @param numberOfLetters - Integer
     * @return
     */
    public String getRandomText(Integer numberOfLetters){
        String randomText = "test";
        String alphabet= "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        for (int i = 0; i < numberOfLetters; i++) {
            char c = alphabet.charAt(random.nextInt(26));
            randomText+=c;
        }
        return randomText;
    }

    /**
     *
     * @param numberOfDigits - Integer
     * @return
     */
    public String getRandomNumber(Integer numberOfDigits){
        String randomDigits = "";
        String digits= "123456789";
        Random random = new Random();
        for (int i = 0; i < numberOfDigits; i++) {
            char c = digits.charAt(random.nextInt(9));
            randomDigits+=c;
        }
        return randomDigits;
    }

    /**
     *
     * @param field - WebElement
     * @param fillInTxt - String
     */
    public void fillInField(WebElement field, String fillInTxt){
        field.clear();
        field.sendKeys(fillInTxt);
    }

    public void fillInAndPressEnter(WebElement field, String fillInTxt){
        field.clear();
        field.sendKeys(fillInTxt);
        field.sendKeys(Keys.ENTER);
    }

    public void pressCtrlEnd(WebDriver driver) {
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
    }

    public void pressPageDown(WebDriver driver) {
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.PAGE_DOWN).perform();
    }

    public void moveToElement(WebDriver driver, WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }

    public static boolean compareURL(WebDriver driver, String expectedURL) {
        String currentURL = driver.getCurrentUrl();
        boolean result = false;

        if (currentURL.contains(expectedURL)) {
            result = true;
        }
        else {
            result = false;
        }
        return result;
    }

    public static String getRandomValueFromArray(String[] array) {
        String rnd = array[new Random().nextInt(array.length)];
        return rnd;
    }

    /**
     * Search in an array of an element and click on it.
     * @param myElement the List WebElement
     * @param myConstants the search String
     */
    public static void searchElementEndGoIt(List<WebElement> myElement, String myConstants) throws Exception {
        for (WebElement element : myElement){
            if (element.getAttribute("class").contains(myConstants)){
                continue;
            }else {
                System.out.println("Team color don't match (" + element.getAttribute("class") + ")");
                throw new Exception();
            }
        }
    }

    /**
     *
     * @param element - WebElement
     * @param value - String
     */
    public void selectByValue(WebElement element, String value){
        Select select = new Select(element);
        select.selectByValue(value);
    }

    /**
     *
     * @param element - WebElement
     * @param text - String
     */
    public void selectByText(WebElement element, String text){
        Select select = new Select(element);
        select.selectByVisibleText(text);
    }

    /**
     *
     * @param element - WebElement
     * @param index - Integer
     */
    public void selectByIndex(WebElement element, Integer index){
        Select select = new Select(element);
        select.selectByIndex(index);
    }

    /**
     *
     * @param driver - WebDriver
     * @param element - WebElement
     */
    public void executeJavascriptClick(WebDriver driver, WebElement element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

    /**
     *
     * @param driver - WebDriver
     * @param element - WebElement
     * @param attribute - String
     * @param value - String
     */
    public void executeJavascriptSetAttribute(WebDriver driver, WebElement element, String attribute, String value){
        ((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute('"+ attribute +"', '" + value +"')", element);
    }

    /**
     *
     * @param driver - WebDriver
     * @param element - WebElement
     * @param text - String
     */
    public void executeJavascriptSendKeys(WebDriver driver, WebElement element, String text){
        ((JavascriptExecutor) driver).executeScript("arguments[0].value='"+ text +"'", element);
    }

    /**
     *
     * @param driver - WebDriver
     * @param element - WebElement
     * @param value - String
     * @return
     */
    public String executeJavascriptGetAttribute(WebDriver driver, WebElement element, String value){
        String valueAttribute = ((JavascriptExecutor) driver).executeScript("arguments[0].getAttribute('"+ value +"')", element).toString();
        return valueAttribute;
    }

    public void dragAndDropElement(WebDriver driver, WebElement element, WebElement elementTo){
        Actions actions = new Actions(driver);
        actions.dragAndDrop(element, elementTo).build().perform();
    }

    public void executeJavascriptStyleDisplay(WebDriver driver, String styleDisplay, WebElement element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.display='" + styleDisplay +"';", element);
    }
}
