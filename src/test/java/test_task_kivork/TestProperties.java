package test_task_kivork;

import java.io.IOException;
import java.util.Properties;

public class TestProperties {
    /** The properties. */
    private static Properties properties = new Properties();
    private static Properties propertiesData = new Properties();
    private static Properties propertiesTestData = new Properties();

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public static Properties getProperties()
    {
        if( properties.size() == 0 )
        {
            try
            {
                properties.load(TestProperties.class.getClassLoader().getResourceAsStream("baseTest.properties") );
            }
            catch (IOException e)
            {
            }
        }
        return properties;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    public static Properties getDataProperties()
    {
        if( propertiesData.size() == 0 )
        {
            try
            {
                propertiesData.load(TestProperties.class.getClassLoader().getResourceAsStream("data.properties") );
            }
            catch (IOException e)
            {
            }
        }
        return propertiesData;
    }

    public static Properties getTestDataProperties()
    {
        if( propertiesTestData.size() == 0 )
        {
            try
            {
                propertiesTestData.load(TestProperties.class.getClassLoader().getResourceAsStream("testData.properties") );
            }
            catch (IOException e)
            {
            }
        }
        return propertiesTestData;
    }
}
