package test_task_kivork.pages;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import test_task_kivork.TestProperties;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.config.RedirectConfig.redirectConfig;

public class IPStackPage extends AbstractPage{

    public IPStackPage(WebDriver driver) {
        super(driver);
    }

    @Step("Send Get request")
    public void sendRequest(){
        RestAssured.baseURI = TestProperties.getProperties().getProperty("apiIPStackUrl");
        RequestSpecification request = given().config(RestAssured.config().redirect(redirectConfig().followRedirects(false)));

        List restAssuredCookies = getCookie();
        System.out.println("restAssuredCookies - " + restAssuredCookies);

        Response response = request.queryParam("access_key", TestProperties.getProperties().getProperty("apiKey")).get("178.150.45.33");

        String jsonBody = response.getBody().asString();
        System.out.println("Body - " + jsonBody);

        String apiData [] = {"ip", "type", "continent_code", "continent_name", "country_code", "country_name", "region_code", "region_name", "city", "zip", "latitude", "longitude", "location.geoname_id", "location.capital", "location.languages[0].code", "location.languages[0].name", "location.languages[0].native", "location.country_flag", "location.country_flag_emoji", "location.country_flag_emoji_unicode", "location.calling_code", "location.is_eu"};

        addFile("apiData");
        for (int i = 0; i < apiData.length; i++){
            addVariableToExistFile("apiData", apiData[i], response.jsonPath().getString(apiData[i]));
        }

        response.then().statusCode(200);
        Assertions.assertEquals("50.4", response.jsonPath().getString("latitude").substring(0,4));
        Assertions.assertEquals("30.5", response.jsonPath().getString("longitude").substring(0,4));
    }

}
